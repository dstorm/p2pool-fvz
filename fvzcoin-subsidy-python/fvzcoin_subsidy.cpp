#include <Python.h>

static const int64_t COIN = 100000000;

double ConvertBitsToDouble(unsigned int nBits){
    int nShift = (nBits >> 24) & 0xff;

    double dDiff =
        (double)0x0000ffff / (double)(nBits & 0x00ffffff);

    while (nShift < 29)
    {
        dDiff *= 256.0;
        nShift++;
    }
    while (nShift > 29)
    {
        dDiff /= 256.0;
        nShift--;
    }

    return dDiff;
}

int64_t static GetBlockBaseValue(int nBits, int nHeight, bool fTestNet = false)
{
    double dDiff =
        (double)0x0000ffff / (double)(nBits & 0x00ffffff);

    /* fixed bug caused diff to not be correctly calculated */
    if(nHeight > 120 || fTestNet) dDiff = ConvertBitsToDouble(nBits);

    int64_t nSubsidy = 1;
           if( nHeight >= 400) { // GPU/ASIC difficulty calc
            // 5678901/(((x+2600)/11)^2)
            nSubsidy = (5678901 / (pow((dDiff+2600.0)/11.0,2.0)));
            if (nSubsidy > 100) nSubsidy = 100;
            if (nSubsidy < 10) nSubsidy = 10;
        } else {
        nSubsidy = 1000;

    }

    // printf("height %u diff %4.2f reward %i \n", nHeight, dDiff, nSubsidy);
    nSubsidy *= COIN/1000;

    // yearly decline of production by 1% per month, projected 34,000-430,000 FVZ coins max by year 2050.
    for(int i = 43800; i <= nHeight; i += 43800) nSubsidy *= 0.99;

    return nSubsidy;
}

static PyObject *fvzcoin_subsidy_getblockbasevalue(PyObject *self, PyObject *args)
{
    int input_bits;
    int input_height;
    if (!PyArg_ParseTuple(args, "ii", &input_bits, &input_height))
        return NULL;
    long long output = GetBlockBaseValue(input_bits, input_height);
    return Py_BuildValue("L", output);
}

static PyObject *fvzcoin_subsidy_getblockbasevalue_testnet(PyObject *self, PyObject *args)
{
    int input_bits;
    int input_height;
    if (!PyArg_ParseTuple(args, "ii", &input_bits, &input_height))
        return NULL;
    long long output = GetBlockBaseValue(input_bits, input_height, true);
    return Py_BuildValue("L", output);
}

static PyMethodDef fvzcoin_subsidy_methods[] = {
    { "GetBlockBaseValue", fvzcoin_subsidy_getblockbasevalue, METH_VARARGS, "Returns the block value" },
    { "GetBlockBaseValue_testnet", fvzcoin_subsidy_getblockbasevalue_testnet, METH_VARARGS, "Returns the block value for testnet" },
    { NULL, NULL, 0, NULL }
};

PyMODINIT_FUNC initfvzcoin_subsidy(void) {
    (void) Py_InitModule("fvzcoin_subsidy", fvzcoin_subsidy_methods);
}
