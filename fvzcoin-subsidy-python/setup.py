from distutils.core import setup, Extension

fvzcoin_module = Extension('fvzcoin_subsidy', sources = ['fvzcoin_subsidy.cpp'])

setup (name = 'fvzcoin_subsidy',
       version = '1.0',
       description = 'Subsidy function for FVZCoin',
       ext_modules = [fvzcoin_module])
